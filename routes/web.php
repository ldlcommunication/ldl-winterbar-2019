<?php

Route::view('/', 'frontend.pages.registration');
Route::view('admin/login', 'backend.auth.login');
Route::view('admin/registrations', 'backend.registrations.index');
Route::view('admin/registration/10/edit', 'backend.registrations.edit');
