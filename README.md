# Test Back-end LDL Communication

Elk jaar geven wij met LDL Communication een Winterdrink voor de bedrijven hier in de buurt.
Voorheen was het voorzien om enkel de werkende mensen hier te laten aan deelnemen maar dit jaar liggen de kaarten anders.
Er zal animatie zijn voor de kinderen en de partners zijn ook allen welkom!

Hiervoor hadden we graag geweten, wie er komt al dan niet met partner en hoeveel kinderen er eventueel deelnemen.
Daarom gaan we een mailing uitsturen met een registratie page.

Als men klikt op deel te nemen zal dit project openen in hun browser. Nu is het aan jou om deze data correct te werken.

    

## Setup
 - Clone dit project naar je computer
 - Nadat je het project hebt geinstalleerd kan je normaal gezien een pagina terugvinden die onze *kerstbar* weergeeft.
 
 ## Actief
 - Wegschrijven van de NAW gegevens
 - Wegscrijven van de partner indien opgegeven
 - Wegschrijven van X aantal kinderen (voornaan, naam, leeftijd) indien opgegeven.
 - GDPR check

## Management
- In resources/backend/ kan u all views terugvinden voor 
    - Login
    - Overview
    - Edit (wijzigen van een registratie)
    

### Volgende views werden voor U reeds aangemaakt.    
``` 
Route::view('/', 'frontend.pages.registration');
Route::view('admin/login', 'backend.auth.login');
Route::view('admin/registrations', 'backend.registrations.index');
Route::view('admin/registration/10/edit', 'backend.registrations.edit');
```
    
Graag hadden we een user : `ward_ldl` gehad met wachtwoord `tester001` die kan inloggen om deze registraties te managen.


```
1. Indien klaar graag de projectfolder (zonder vendor/node_modules folder) zippen + SQL Export.
2. NaamVoornaam_LDL_BackendTest.zip
3. Graag deze file uploaden via WeTransfer en versturen naar: ward@ldl.be & annesophie@ldl.be
``` 
