(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["vue-admin-components"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/frontend/js/components/Children/Child.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/frontend/js/components/Children/Child.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Child",
  data: function data() {
    return {
      errors: [],
      counter: 0,
      child_firstname: null,
      child_lastname: null,
      child_age: null
    };
  },
  methods: {
    emitChild: function emitChild() {
      if (this.child_firstname === null) {
        this.errors.push('Gelieve een voornaam op te geven');
      }

      if (this.child_lastname === null) {
        this.errors.push('Gelieve een achternaam op te geven');
      }

      if (this.child_age === null) {
        this.errors.push('Gelieve een leeftijd op te geven');
      }

      if (this.errors.length > 0) {// There are errors...
      } else {
        this.$emit("input", {
          child_firstname: this.$data.child_firstname,
          child_lastname: this.$data.child_lastname,
          child_age: this.$data.child_age
        });
        this.child_firstname = null;
        this.child_lastname = null;
        this.child_age = null;
      } // RESET
      // this.errors = [];

    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/frontend/js/components/Partner.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/frontend/js/components/Partner.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Partner",
  data: function data() {
    return {
      partner_firstname: null,
      partner_lastname: null
    };
  },
  methods: {
    emitChange: function emitChange() {
      this.$emit("input", {
        partner_firstname: this.$data.partner_firstname,
        partner_lastname: this.$data.partner_lastname
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/frontend/js/components/Registration.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/frontend/js/components/Registration.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Partner__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Partner */ "./resources/frontend/js/components/Partner.vue");
/* harmony import */ var _Children_Child__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Children/Child */ "./resources/frontend/js/components/Children/Child.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Registration",
  data: function data() {
    return {
      lastname: 'Kennes',
      firstname: 'Ward',
      street: 'Binnenweg',
      no: '25',
      bus: null,
      zip: '2570',
      city: 'Duffel',
      email: 'ward@ldl.be',
      company: 'LDL Communication',
      partner: false,
      partner_data: [],
      children: false,
      children_data: [],
      logo: null,
      gdpr: null
    };
  },
  methods: {
    fetchParentData: function fetchParentData(val) {
      this.partner_data = val;
    },
    fetchChildData: function fetchChildData(val) {
      this.children_data.push(val);
    },
    onsubmit: function onsubmit() {
      //
      // Front-end validation should be applied but this is a testcase w/o.
      //
      this.$http.post('register', {
        lastname: this.lastname,
        firstname: this.firstname,
        street: this.street,
        no: this.no,
        bus: this.bus,
        zip: this.zip,
        city: this.city,
        email: this.email,
        company: this.company,
        partner_data: this.partner_data,
        children_data: this.children_data
      }).then(function (response) {
        console.log(response);
      })["catch"](function (error) {
        console.log(error);
      });
    }
  },
  mounted: function mounted() {
    console.log('Mounted registration component.');
  },
  components: {
    'partner': _Partner__WEBPACK_IMPORTED_MODULE_0__["default"],
    'child': _Children_Child__WEBPACK_IMPORTED_MODULE_1__["default"]
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/frontend/js/components/Registration.vue?vue&type=style&index=0&id=d6ecdcfe&scoped=true&lang=scss&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/frontend/js/components/Registration.vue?vue&type=style&index=0&id=d6ecdcfe&scoped=true&lang=scss& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".child[data-v-d6ecdcfe] {\n  border-radius: 8px;\n  padding: 15px 15px;\n  background: #9dc1d4;\n  color: #ffffff;\n  font-weight: 100;\n  margin-bottom: 8px;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/frontend/js/components/Registration.vue?vue&type=style&index=0&id=d6ecdcfe&scoped=true&lang=scss&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/frontend/js/components/Registration.vue?vue&type=style&index=0&id=d6ecdcfe&scoped=true&lang=scss& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--8-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./Registration.vue?vue&type=style&index=0&id=d6ecdcfe&scoped=true&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/frontend/js/components/Registration.vue?vue&type=style&index=0&id=d6ecdcfe&scoped=true&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/frontend/js/components/Children/Child.vue?vue&type=template&id=cf8e4f98&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/frontend/js/components/Children/Child.vue?vue&type=template&id=cf8e4f98&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "container" }, [
      _vm.errors
        ? _c(
            "div",
            { staticClass: "row errors" },
            _vm._l(_vm.errors, function(error) {
              return _c(
                "div",
                { staticClass: "alert alert-danger d-block w-100" },
                [_c("p", [_vm._v(_vm._s(error))])]
              )
            }),
            0
          )
        : _vm._e()
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col" }, [
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.child_firstname,
              expression: "child_firstname"
            }
          ],
          staticClass: "form-control",
          attrs: {
            type: "text",
            placeholder: "Firstname child",
            autocomplete: "off"
          },
          domProps: { value: _vm.child_firstname },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.child_firstname = $event.target.value
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col" }, [
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.child_lastname,
              expression: "child_lastname"
            }
          ],
          staticClass: "form-control",
          attrs: {
            type: "text",
            placeholder: "Lastname child",
            autocomplete: "off"
          },
          domProps: { value: _vm.child_lastname },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.child_lastname = $event.target.value
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col" }, [
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.child_age,
              expression: "child_age"
            }
          ],
          staticClass: "form-control",
          attrs: {
            type: "text",
            placeholder: "Age child",
            autocomplete: "off"
          },
          domProps: { value: _vm.child_age },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.child_age = $event.target.value
            }
          }
        })
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col clearfix" }, [
        _c(
          "button",
          {
            staticClass: "btn btn-outline-dark float-right",
            on: {
              click: function($event) {
                $event.preventDefault()
                return _vm.emitChild($event)
              }
            }
          },
          [_vm._v("Add Child")]
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/frontend/js/components/Partner.vue?vue&type=template&id=7ece02b0&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/frontend/js/components/Partner.vue?vue&type=template&id=7ece02b0& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row" }, [
    _c("div", { staticClass: "col" }, [
      _c("label", { attrs: { for: "partner_lastname" } }, [
        _vm._v("Partner lastname")
      ]),
      _vm._v(" "),
      _c("input", {
        directives: [
          {
            name: "model",
            rawName: "v-model",
            value: _vm.partner_firstname,
            expression: "partner_firstname"
          }
        ],
        staticClass: "form-control",
        attrs: { type: "text", id: "partner_lastname" },
        domProps: { value: _vm.partner_firstname },
        on: {
          change: _vm.emitChange,
          input: function($event) {
            if ($event.target.composing) {
              return
            }
            _vm.partner_firstname = $event.target.value
          }
        }
      })
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col" }, [
      _c("label", { attrs: { for: "partner_firstname" } }, [
        _vm._v("Partner Lastname")
      ]),
      _vm._v(" "),
      _c("input", {
        directives: [
          {
            name: "model",
            rawName: "v-model",
            value: _vm.partner_lastname,
            expression: "partner_lastname"
          }
        ],
        staticClass: "form-control ",
        attrs: { type: "text", id: "partner_firstname" },
        domProps: { value: _vm.partner_lastname },
        on: {
          change: _vm.emitChange,
          input: function($event) {
            if ($event.target.composing) {
              return
            }
            _vm.partner_lastname = $event.target.value
          }
        }
      })
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/frontend/js/components/Registration.vue?vue&type=template&id=d6ecdcfe&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/frontend/js/components/Registration.vue?vue&type=template&id=d6ecdcfe&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "container-fluid bg-primary pb-5" }, [
      _c("div", { staticClass: "container" }, [
        _c("h2", [_vm._v("LDL Winterbar")]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "Registreer u en uw familie hieronder om deel te nemen aan de LDL Winterbar 2019."
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col" }, [
            _c("label", { attrs: { for: "lastname" } }, [_vm._v("Lastname")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.lastname,
                  expression: "lastname"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", id: "lastname" },
              domProps: { value: _vm.lastname },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.lastname = $event.target.value
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col" }, [
            _c("label", { attrs: { for: "firstname" } }, [_vm._v("Firstname")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.firstname,
                  expression: "firstname"
                }
              ],
              staticClass: "form-control ",
              attrs: { type: "text", id: "firstname" },
              domProps: { value: _vm.firstname },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.firstname = $event.target.value
                }
              }
            })
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col" }, [
              _c("label", { attrs: { for: "street" } }, [_vm._v("Street")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.street,
                    expression: "street"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "text", id: "street" },
                domProps: { value: _vm.street },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.street = $event.target.value
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col" }, [
              _c("label", { attrs: { for: "no" } }, [_vm._v("Number")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.no,
                    expression: "no"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "text", id: "no" },
                domProps: { value: _vm.no },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.no = $event.target.value
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col" }, [
              _c("label", { attrs: { for: "bus" } }, [_vm._v("Bus")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.bus,
                    expression: "bus"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "text", id: "bus" },
                domProps: { value: _vm.bus },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.bus = $event.target.value
                  }
                }
              })
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col" }, [
              _c("label", { attrs: { for: "zip" } }, [_vm._v("ZIP")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.zip,
                    expression: "zip"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "text", id: "zip" },
                domProps: { value: _vm.zip },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.zip = $event.target.value
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col" }, [
              _c("label", { attrs: { for: "zip" } }, [_vm._v("City")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.city,
                    expression: "city"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "text", id: "city" },
                domProps: { value: _vm.city },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.city = $event.target.value
                  }
                }
              })
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col" }, [
              _c("label", { attrs: { for: "email" } }, [_vm._v("E-mail")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.email,
                    expression: "email"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "text", id: "email" },
                domProps: { value: _vm.email },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.email = $event.target.value
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col" }, [
              _c("label", { attrs: { for: "zip" } }, [_vm._v("Company")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.company,
                    expression: "company"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "text", id: "company" },
                domProps: { value: _vm.company },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.company = $event.target.value
                  }
                }
              })
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "container pt-5" }, [
      _c("div", { staticClass: "data_options mb-3" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col" }, [
            _c("div", { staticClass: "form-group" }, [
              _vm._m(0),
              _c("br"),
              _vm._v(" "),
              _c("label", { attrs: { for: "partner_no" } }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.partner,
                      expression: "partner"
                    }
                  ],
                  attrs: { type: "radio", id: "partner_no" },
                  domProps: {
                    value: false,
                    checked: _vm._q(_vm.partner, false)
                  },
                  on: {
                    change: function($event) {
                      _vm.partner = false
                    }
                  }
                }),
                _vm._v(
                  "\n                        Nee, ik kom alleen\n                    "
                )
              ]),
              _c("br"),
              _vm._v(" "),
              _c("label", { attrs: { for: "partner_yes" } }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.partner,
                      expression: "partner"
                    }
                  ],
                  attrs: { type: "radio", id: "partner_yes" },
                  domProps: { value: true, checked: _vm._q(_vm.partner, true) },
                  on: {
                    change: function($event) {
                      _vm.partner = true
                    }
                  }
                }),
                _vm._v(
                  "\n                        Ja, ik kom met partner\n                    "
                )
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col" }, [
            _vm.partner
              ? _c(
                  "div",
                  [_c("partner", { on: { input: _vm.fetchParentData } })],
                  1
                )
              : _vm._e()
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "container pt-5" }, [
        _c("div", { staticClass: "data_options mb-3" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col" }, [
              _c("div", { staticClass: "form-group" }, [
                _vm._m(1),
                _c("br"),
                _vm._v(" "),
                _c("label", { attrs: { for: "children_no" } }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.children,
                        expression: "children"
                      }
                    ],
                    attrs: { type: "radio", id: "children_no" },
                    domProps: {
                      value: false,
                      checked: _vm._q(_vm.children, false)
                    },
                    on: {
                      change: function($event) {
                        _vm.children = false
                      }
                    }
                  }),
                  _vm._v(
                    "\n                            Nee, ik kom alleen\n                        "
                  )
                ]),
                _c("br"),
                _vm._v(" "),
                _c("label", { attrs: { for: "children_yes" } }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.children,
                        expression: "children"
                      }
                    ],
                    attrs: { type: "radio", id: "children_yes" },
                    domProps: {
                      value: true,
                      checked: _vm._q(_vm.children, true)
                    },
                    on: {
                      change: function($event) {
                        _vm.children = true
                      }
                    }
                  }),
                  _vm._v(
                    "\n                            Ja, ik kom met kind(eren)\n                        "
                  )
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _vm.children
              ? _c(
                  "div",
                  { staticClass: "col" },
                  [
                    _vm._l(_vm.children_data, function(child) {
                      return _c("div", { staticClass: "child" }, [
                        _c("strong", [_vm._v(_vm._s(child.child_firstname))]),
                        _vm._v(" "),
                        _c("strong", [_vm._v(_vm._s(child.child_lastname))]),
                        _vm._v(" "),
                        _c("strong", [_vm._v(_vm._s(child.child_age))])
                      ])
                    }),
                    _vm._v(" "),
                    _c("child", { on: { input: _vm.fetchChildData } })
                  ],
                  2
                )
              : _vm._e()
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "data_options mb-3" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "gdpr" } }),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.gdpr,
                    expression: "gdpr"
                  }
                ],
                attrs: { type: "checkbox", value: "1", id: "gdpr" },
                domProps: {
                  checked: Array.isArray(_vm.gdpr)
                    ? _vm._i(_vm.gdpr, "1") > -1
                    : _vm.gdpr
                },
                on: {
                  change: function($event) {
                    var $$a = _vm.gdpr,
                      $$el = $event.target,
                      $$c = $$el.checked ? true : false
                    if (Array.isArray($$a)) {
                      var $$v = "1",
                        $$i = _vm._i($$a, $$v)
                      if ($$el.checked) {
                        $$i < 0 && (_vm.gdpr = $$a.concat([$$v]))
                      } else {
                        $$i > -1 &&
                          (_vm.gdpr = $$a
                            .slice(0, $$i)
                            .concat($$a.slice($$i + 1)))
                      }
                    } else {
                      _vm.gdpr = $$c
                    }
                  }
                }
              }),
              _vm._v(" U gaat akkoord met ons "),
              _c("strong", [_vm._v("privacy beleid")]),
              _vm._v("?\n                    ")
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "data_options mb-3" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("input", {
                staticClass: "btn btn-block btn-outline-primary float-right",
                attrs: { type: "submit", value: "Verzeden" },
                on: { click: _vm.onsubmit }
              })
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("strong", [
      _vm._v("Zal u aanwezig zijn samen met uw "),
      _c("span", [_vm._v("partner")]),
      _vm._v("?")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("strong", [
      _vm._v("Zal u aanwezig zijn samen met uw "),
      _c("span", [_vm._v("kinderen")]),
      _vm._v("?")
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/frontend/js/components/Children/Child.vue":
/*!*************************************************************!*\
  !*** ./resources/frontend/js/components/Children/Child.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Child_vue_vue_type_template_id_cf8e4f98_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Child.vue?vue&type=template&id=cf8e4f98&scoped=true& */ "./resources/frontend/js/components/Children/Child.vue?vue&type=template&id=cf8e4f98&scoped=true&");
/* harmony import */ var _Child_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Child.vue?vue&type=script&lang=js& */ "./resources/frontend/js/components/Children/Child.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Child_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Child_vue_vue_type_template_id_cf8e4f98_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Child_vue_vue_type_template_id_cf8e4f98_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "cf8e4f98",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/frontend/js/components/Children/Child.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/frontend/js/components/Children/Child.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/frontend/js/components/Children/Child.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Child_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Child.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/frontend/js/components/Children/Child.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Child_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/frontend/js/components/Children/Child.vue?vue&type=template&id=cf8e4f98&scoped=true&":
/*!********************************************************************************************************!*\
  !*** ./resources/frontend/js/components/Children/Child.vue?vue&type=template&id=cf8e4f98&scoped=true& ***!
  \********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Child_vue_vue_type_template_id_cf8e4f98_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Child.vue?vue&type=template&id=cf8e4f98&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/frontend/js/components/Children/Child.vue?vue&type=template&id=cf8e4f98&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Child_vue_vue_type_template_id_cf8e4f98_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Child_vue_vue_type_template_id_cf8e4f98_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/frontend/js/components/Partner.vue":
/*!******************************************************!*\
  !*** ./resources/frontend/js/components/Partner.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Partner_vue_vue_type_template_id_7ece02b0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Partner.vue?vue&type=template&id=7ece02b0& */ "./resources/frontend/js/components/Partner.vue?vue&type=template&id=7ece02b0&");
/* harmony import */ var _Partner_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Partner.vue?vue&type=script&lang=js& */ "./resources/frontend/js/components/Partner.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Partner_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Partner_vue_vue_type_template_id_7ece02b0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Partner_vue_vue_type_template_id_7ece02b0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/frontend/js/components/Partner.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/frontend/js/components/Partner.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/frontend/js/components/Partner.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Partner_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Partner.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/frontend/js/components/Partner.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Partner_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/frontend/js/components/Partner.vue?vue&type=template&id=7ece02b0&":
/*!*************************************************************************************!*\
  !*** ./resources/frontend/js/components/Partner.vue?vue&type=template&id=7ece02b0& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Partner_vue_vue_type_template_id_7ece02b0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Partner.vue?vue&type=template&id=7ece02b0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/frontend/js/components/Partner.vue?vue&type=template&id=7ece02b0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Partner_vue_vue_type_template_id_7ece02b0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Partner_vue_vue_type_template_id_7ece02b0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/frontend/js/components/Registration.vue":
/*!***********************************************************!*\
  !*** ./resources/frontend/js/components/Registration.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Registration_vue_vue_type_template_id_d6ecdcfe_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Registration.vue?vue&type=template&id=d6ecdcfe&scoped=true& */ "./resources/frontend/js/components/Registration.vue?vue&type=template&id=d6ecdcfe&scoped=true&");
/* harmony import */ var _Registration_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Registration.vue?vue&type=script&lang=js& */ "./resources/frontend/js/components/Registration.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Registration_vue_vue_type_style_index_0_id_d6ecdcfe_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Registration.vue?vue&type=style&index=0&id=d6ecdcfe&scoped=true&lang=scss& */ "./resources/frontend/js/components/Registration.vue?vue&type=style&index=0&id=d6ecdcfe&scoped=true&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Registration_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Registration_vue_vue_type_template_id_d6ecdcfe_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Registration_vue_vue_type_template_id_d6ecdcfe_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "d6ecdcfe",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/frontend/js/components/Registration.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/frontend/js/components/Registration.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/frontend/js/components/Registration.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Registration_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Registration.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/frontend/js/components/Registration.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Registration_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/frontend/js/components/Registration.vue?vue&type=style&index=0&id=d6ecdcfe&scoped=true&lang=scss&":
/*!*********************************************************************************************************************!*\
  !*** ./resources/frontend/js/components/Registration.vue?vue&type=style&index=0&id=d6ecdcfe&scoped=true&lang=scss& ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Registration_vue_vue_type_style_index_0_id_d6ecdcfe_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--8-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./Registration.vue?vue&type=style&index=0&id=d6ecdcfe&scoped=true&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/frontend/js/components/Registration.vue?vue&type=style&index=0&id=d6ecdcfe&scoped=true&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Registration_vue_vue_type_style_index_0_id_d6ecdcfe_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Registration_vue_vue_type_style_index_0_id_d6ecdcfe_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Registration_vue_vue_type_style_index_0_id_d6ecdcfe_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Registration_vue_vue_type_style_index_0_id_d6ecdcfe_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Registration_vue_vue_type_style_index_0_id_d6ecdcfe_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/frontend/js/components/Registration.vue?vue&type=template&id=d6ecdcfe&scoped=true&":
/*!******************************************************************************************************!*\
  !*** ./resources/frontend/js/components/Registration.vue?vue&type=template&id=d6ecdcfe&scoped=true& ***!
  \******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Registration_vue_vue_type_template_id_d6ecdcfe_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Registration.vue?vue&type=template&id=d6ecdcfe&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/frontend/js/components/Registration.vue?vue&type=template&id=d6ecdcfe&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Registration_vue_vue_type_template_id_d6ecdcfe_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Registration_vue_vue_type_template_id_d6ecdcfe_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);