import axios from './services/axios'

import(/* webpackChunkName: 'vendor-vue' */ 'vue').then(({ default: Vue }) => {

    Vue.prototype.$http = axios;

    window.vm = new Vue({
        el: '#app',

        components: {
            Registration: () => import(/* webpackChunkName: 'vue-frontend-components' */ '../js/components/Registration'),
            Partner: () => import(/* webpackChunkName: 'vue-frontend-components' */ '../js/components/Partner'),
            Child: () => import(/* webpackChunkName: 'vue-frontend-components' */ '../js/components/Children/Child'),
        },
        computed: {
        },
        methods: {
        },
        mounted () {
        }
    });
});
