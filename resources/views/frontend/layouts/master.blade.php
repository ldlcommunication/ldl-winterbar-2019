<!DOCTYPE html>
<html>
    <head>
        <title>{{ config('app.name') }}</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="LDL Communication">
        <meta name="robots" content="noindex">
        <link rel="stylesheet" href="{{ mix('/frontend/css/app.css') }}">
    </head>

    <body>
        <div id="app">
            @yield('content')
        </div>
        <script src="{{ mix('/frontend/js/app.js') }}"></script>
    </body>
</html>
