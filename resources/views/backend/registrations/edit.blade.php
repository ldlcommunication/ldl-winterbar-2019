@extends('backend.layouts.master')
@section('content')
    <div class="container">
        <h2>Registratie 10 wijzigen</h2>
        <p>U kan hier registratie 10 wijzigen.</p>

        <form action="#">
            @csrf
            <fieldset class="bg-light p-3">
                <h5>Algemene gegevens:</h5>
            <div class="row">
                <div class="col">
                    <div class="form-group">Lastname <input type="text" value="" class="form-control"> </div>
                </div>
                <div class="col">
                    <div class="form-group">Firstname <input type="text"  value="" class="form-control"> </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-group">Street <input type="text"  value="" class="form-control"> </div>
                </div>
                <div class="col">
                    <div class="form-group">Number <input type="text"  value="" class="form-control"> </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-group">Bus <input type="text"  value="" class="form-control"> </div>
                </div>
                <div class="col">
                    <div class="form-group">Zip <input type="text"  value="" class="form-control"> </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-group">City <input type="text"  value="" class="form-control"> </div>
                </div>
                <div class="col">
                    <div class="form-group">E-mail <input type="text"  value="" class="form-control"> </div>
                </div>
            </div>


            <div class="row">
                <div class="col">
                    <div class="form-group">Company <input type="text"  value="" class="form-control"> </div>
                </div>
            </div>
            </fieldset>

            <fieldset class="bg-light p-3 mt-4">
                <h5>Partner details:</h5>
                <div class="row">
                    <div class="col">
                        <div class="form-group">Partner firstname<input type="text" class="form-control"></div>
                    </div>
                    <div class="col">
                        <div class="form-group">Partner lastname<input type="text" class="form-control"></div>
                    </div>
                </div>
            </fieldset>


            <fieldset class="bg-light p-3 mt-4">
                <h5>Children details:</h5>

                <div class="current_childs">
                    <label>Selecteer de kinderen die u wil weggooien:</label>

                    <div class="form-check">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="child_ids[]">Ward Kennes - 5 jaar</label>
                    </div>

                    <div class="form-check">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="child_ids[]">Anne-Sophie Verbeeck - 4 jaar</label>
                    </div>

                    <div class="form-check">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="child_ids[]">Egwin Asman - 14 jaar</label>
                    </div>
                </div>

                <hr>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <div class="">Voornaam: <input type="text" class="form-control"></div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="form-group">
                            <div class="">Achternaam: <input type="text" class="form-control"></div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="form-group">
                            <div class="">Leeftijd: <input type="number" class="form-control"></div>
                        </div>
                    </div>
                    <div class="col">
                        <span>&nbsp;<br></span>
                        <a href="#" class="btn btn-secondary">Kind toevoegen</a>
                    </div>
                </div>
            </fieldset>


            <hr>
            <div class="row">

                <div class="col"><input type="submit"  class="btn btn-secondary d-block w-100" value="Update"></div>
            </div>
        </form>
    </div>
@endsection
