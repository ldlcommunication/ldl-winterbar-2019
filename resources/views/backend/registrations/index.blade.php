@extends('backend.layouts.master')
@section('content')
    <div class="container">
        <h2>Registratie overzicht</h2>
        <p>U kan hier de registraties raadplegen/wijzigen/verwijderen.</p>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Naam/voornaam</th>
                    <th>Bedrijf</th>
                    <th>Met partner</th>
                    <th>Aantal kinderen</th>
                    <th>Acties</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Kennes Ward</td>
                    <td>LDL Communication</td>
                    <td>Ja</td>
                    <td>1</td>
                    <td>
                        <a href="/admin/registration/10/edit" class="btn btn-sm btn-warning">Edit</a>
                        <a href="#" class="btn btn-sm btn-secondary">Delete</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
