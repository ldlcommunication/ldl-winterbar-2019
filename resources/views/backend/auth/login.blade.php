@extends('backend.layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-6 mx-sm-auto col-md-8 ">
            <h4>Inloggen</h4>
            <form action="#" method="post">
                @csrf

                <div class="form-group">
                    <input type="username" class="form-control" required tabindex="1" placeholder="Username">
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" required tabindex="2" placeholder="Wachtwoord">
                </div>

                <input type="submit" value="Inloggen" class="btn btn-secondary">
            </form>
        </div>
    </div>
</div>
@endsection
