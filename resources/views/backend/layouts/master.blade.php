<!DOCTYPE html>
<html>
<head>
    <title>{{ config('app.name') }} - Admin</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="LDL Communication">
    <meta name="robots" content="noindex">
    <link rel="stylesheet" href="{{ mix('/backend/css/app.css') }}">
</head>

<body>

    @include('backend.components.nav')
    @yield('content')

    <script src="{{ mix('/backend/js/app.js') }}"></script>
</body>
</html>
